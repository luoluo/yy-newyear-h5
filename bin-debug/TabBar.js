/**
 * ★舞台下部操作tab操作区域★
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var tabBar = (function (_super) {
    __extends(tabBar, _super);
    function tabBar(index) {
        if (index === void 0) { index = 0; }
        var _this = _super.call(this) || this;
        //按钮对象组
        _this.btnList = [];
        _this.width = LL.Config.stageWidth;
        _this.height = LL.Config.tabHeight;
        _this.top = 0;
        _this.left = 0;
        //背景
        var bg = new egret.Shape();
        bg.graphics.beginFill(0xf5f5f5, 1);
        bg.graphics.drawRect(0, 0, _this.width, _this.height);
        bg.graphics.endFill();
        _this.addChild(bg);
        _this.createTabBtn();
        _this.createFold();
        _this.focusIndex = index;
        _this.setFocusState(index);
        return _this;
    }
    //创建5个按钮
    tabBar.prototype.createTabBtn = function () {
        var _this = this;
        var bgBtn = new tabBtn("icon_sofa.fac75_png");
        this.addChild(bgBtn);
        bgBtn.onFocus(function () {
            _this.setFocusState(0);
        });
        this.btnList.push(bgBtn);
        var personBtn = new tabBtn("icon_adult.f458f_png");
        this.addChild(personBtn);
        this.btnList.push(personBtn);
        personBtn.onFocus(function () {
            _this.setFocusState(1);
        });
        var babyBtn = new tabBtn("icon_kid.c28dd_png");
        this.addChild(babyBtn);
        this.btnList.push(babyBtn);
        babyBtn.onFocus(function () {
            _this.setFocusState(2);
        });
        var catBtn = new tabBtn("icon_cat.d56f8_png");
        this.addChild(catBtn);
        this.btnList.push(catBtn);
        catBtn.onFocus(function () {
            _this.setFocusState(3);
        });
        //布局
        var layout = new eui.HorizontalLayout();
        layout.gap = 0;
        layout.horizontalAlign = egret.HorizontalAlign.LEFT;
        this.layout = layout;
    };
    //设置焦点状态
    tabBar.prototype.setFocusState = function (index) {
        if (this.focusIndex >= 0) {
            this.btnList[this.focusIndex].setBlur();
        }
        this.focusIndex = index;
        this.btnList[this.focusIndex].setFocus();
        if (this.switch) {
            this.switch(index);
        }
    };
    //绑定变换回调
    tabBar.prototype.onSwitch = function (callback) {
        this.switch = callback;
    };
    //折叠按钮
    tabBar.prototype.createFold = function () {
        var _this = this;
        var fold = new tabBtn("mini.83784_png");
        fold.includeInLayout = false;
        fold.anchorOffsetX = fold.width / 2;
        fold.anchorOffsetY = fold.height / 2;
        fold.x = LL.Config.stageWidth - fold.anchorOffsetX;
        fold.y = fold.anchorOffsetY;
        fold.touchEnabled = true;
        fold.addEventListener(egret.TouchEvent.TOUCH_TAP, function (e) {
            fold.rotation += 180;
            _this.hide();
        }, this);
        this.addChild(fold);
    };
    tabBar.prototype.onHide = function (callback) {
        this.hide = callback;
    };
    return tabBar;
}(eui.Group));
__reflect(tabBar.prototype, "tabBar");
