/**
 * ★人元素列表★
 * @param sex 性别
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var personElementBox = (function (_super) {
    __extends(personElementBox, _super);
    function personElementBox(gender, width, height) {
        var _this = _super.call(this) || this;
        //激活的tab序号
        _this.activeIndex = 0;
        //列表元素缓存
        _this.elements = [];
        _this.width = width;
        _this.height = height;
        _this.visible = false;
        _this.x = 0;
        _this.y = LL.Config.tabHeight;
        _this.createTab(gender, LL.Config.stageWidth - LL.Config.personTabBtnWidth, height);
        return _this;
    }
    personElementBox.prototype.createTab = function (gender, width, height) {
        var _this = this;
        var TabTar = new personTabBar(height);
        this.addChild(TabTar);
        //初始化5个列表
        var hairList = new elementList("hair", width, height, gender);
        hairList.x = LL.Config.personTabBtnWidth;
        hairList.y = 0;
        this.elements.push(hairList);
        this.addChild(hairList);
        var glassesList = new elementList("glasses", width, height, gender);
        glassesList.x = LL.Config.personTabBtnWidth,
            glassesList.y = 0;
        this.elements.push(glassesList);
        this.addChild(glassesList);
        var beardList = new elementList("beard", width, height, gender);
        beardList.x = LL.Config.personTabBtnWidth;
        beardList.y = 0;
        this.elements.push(beardList);
        this.addChild(beardList);
        var headList = new elementList("head", width, height, gender);
        headList.x = LL.Config.personTabBtnWidth;
        headList.y = 0;
        this.elements.push(headList);
        this.addChild(headList);
        var bodyList = new elementList("body", width, height, gender);
        bodyList.x = LL.Config.personTabBtnWidth;
        bodyList.y = 0;
        this.elements.push(bodyList);
        this.addChild(bodyList);
        var legList = new elementList("leg", width, height, gender);
        legList.x = LL.Config.personTabBtnWidth;
        legList.y = 0;
        this.elements.push(legList);
        this.addChild(legList);
        TabTar.onSwitch(function (index) {
            _this.switchTab(index);
        });
        hairList.setState(true);
    };
    //变换tab事件
    personElementBox.prototype.switchTab = function (index) {
        if (this.activeIndex >= 0) {
            this.elements[this.activeIndex].setState(false);
        }
        this.activeIndex = index;
        this.elements[index].setState(true);
    };
    personElementBox.prototype.setState = function (visible) {
        this.visible = visible;
    };
    return personElementBox;
}(eui.Group));
__reflect(personElementBox.prototype, "personElementBox");
