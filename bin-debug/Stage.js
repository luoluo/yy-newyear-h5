/**
 * ★H5主要舞台区域场景★
 * 绘制各种元素的区域
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var stage = (function (_super) {
    __extends(stage, _super);
    function stage() {
        var _this = _super.call(this) || this;
        _this.width = LL.Config.stageWidth;
        _this.height = LL.Config.stageHeight;
        _this.mask = new egret.Rectangle(0, 0, _this.width, _this.height);
        _this.drawBg();
        _this.drawCamera();
        //绑定事件
        _this.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
            if (_this.currentItem && evt.target === _this) {
                _this.currentItem.setblur();
            }
        }, _this);
        LL.GlobalEvents.addEvents("addElement", function (e) {
            _this.addElement(e);
        });
        LL.GlobalEvents.addEvents("changePerson", function (e) {
            if (_this.currentItem && _this.currentItem.changePerson) {
                _this.currentItem.changePerson(e);
            }
        });
        return _this;
    }
    // 背景
    stage.prototype.drawBg = function () {
        var bgData = RES.getRes("Room.adb50_png");
        var bg = new egret.Bitmap(bgData);
        bg.x = 0;
        bg.y = 0;
        bg.width = this.width;
        bg.height = this.height;
        this.addChild(bg);
    };
    // 相机
    stage.prototype.drawCamera = function () {
        var _this = this;
        var cameraData = RES.getRes("icon_camera.8e92f_png");
        var camera = new egret.Bitmap(cameraData);
        camera.x = this.width - 90;
        camera.y = LL.Config.stageDisplayHeight - 90;
        camera.width = 80;
        camera.height = 80;
        camera.touchEnabled = true;
        camera.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
            evt.stopImmediatePropagation();
            evt.stopPropagation();
            camera.visible = false;
            var screenShot = new egret.RenderTexture;
            screenShot.drawToTexture(_this);
            _this.photoImg = screenShot;
            camera.visible = true;
        }, this);
        this.addChild(camera);
    };
    // 添加元素
    stage.prototype.addElement = function (e) {
        var _this = this;
        var item = new moveGroup(e);
        item.onDelTap(function () {
            _this.removeChild(item);
        });
        item.onTap(function () {
            if (_this.currentItem && _this.currentItem != item) {
                _this.currentItem.setblur();
            }
            if (e.groupName !== "background") {
                _this.setChildIndex(item, _this.numChildren - 2);
            }
            _this.currentItem = item;
        });
        this.addChild(item);
        this.setChildIndex(item, this.numChildren - 2);
    };
    return stage;
}(eui.Group));
__reflect(stage.prototype, "stage");
