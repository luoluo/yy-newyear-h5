var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * ★内容对象★
 * 内容是一个图片，图片居中，容器宽高与图片相同
 * @param name 图片名称
 */
var container = (function (_super) {
    __extends(container, _super);
    function container(name) {
        var _this = _super.call(this) || this;
        var imageData = RES.getRes(name);
        var image = new egret.Bitmap(imageData);
        _this.width = image.width = imageData.textureWidth / 2;
        _this.height = image.height = imageData.textureHeight / 2;
        _this.anchorOffsetX = _this.width / 2;
        _this.anchorOffsetY = _this.height / 2;
        _this.horizontalCenter = 0;
        _this.verticalCenter = 0;
        _this.addChild(image);
        return _this;
    }
    return container;
}(eui.Group));
__reflect(container.prototype, "container");
