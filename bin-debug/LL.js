/***********************
======▶项目库文件◀======
========★落落★========
***********************/
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
//====★全局事件★====//
var globalEvents = (function () {
    function globalEvents() {
        this.Events = {};
    }
    //添加事件
    globalEvents.prototype.addEvents = function (name, callback) {
        if (!this.Events[name]) {
            this.Events[name] = [];
        }
        this.Events[name].push(callback);
    };
    //触发事件
    globalEvents.prototype.emitEvents = function (name, parameter) {
        if (parameter === void 0) { parameter = {}; }
        if (!this.Events[name]) {
            console.error(name + "事件未注册");
            return;
        }
        this.Events[name].map(function (callback) {
            callback(parameter);
        });
    };
    //删除事件
    globalEvents.prototype.removeEvents = function (name) {
        if (this.Events[name]) {
            delete this.Events[name];
        }
    };
    return globalEvents;
}());
__reflect(globalEvents.prototype, "globalEvents");
var LL = {
    //====★根据父元素尺寸计算图片宽高★====//
    getImageSize: function (width, height, maxWidth, maxHeight, mode) {
        if (mode === void 0) { mode = "scale"; }
        switch (mode) {
            case "scale":
            default:
                var ratio = width / height;
                var maxRatio = maxWidth / maxHeight;
                if (ratio > maxRatio) {
                    return {
                        width: maxWidth,
                        height: height * maxWidth / width
                    };
                }
                else {
                    return {
                        width: width * maxHeight / height,
                        height: maxHeight
                    };
                }
        }
    },
    //====★全局事件★====//
    GlobalEvents: new globalEvents(),
    //全局配置
    Config: {
        stageWidth: 750,
        stageHeight: 974,
        stageDisplayHeight: 800,
        tabWidth: 96,
        tabHeight: 80,
        personTabBtnWidth: 96,
        personTabBtnHeight: 96,
    }
};
