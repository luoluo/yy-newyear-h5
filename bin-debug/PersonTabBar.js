/**
 * ★舞台下部操作tab操作区域★
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var personTabBar = (function (_super) {
    __extends(personTabBar, _super);
    function personTabBar(height) {
        var _this = _super.call(this) || this;
        //焦点序号
        _this.focusIndex = 0;
        //按钮对象组
        _this.btnList = [];
        _this.width = LL.Config.personTabBtnWidth;
        _this.scrollPolicyH = "ScrollPolicy.OFF";
        _this.createTabBtn(height);
        _this.setFocusState(_this.focusIndex);
        return _this;
    }
    //创建6个按钮
    personTabBar.prototype.createTabBtn = function (height) {
        var _this = this;
        //布局
        var group = new eui.Group();
        group.height = height;
        //背景
        var bg = new egret.Shape();
        bg.graphics.beginFill(0xfafafa, 1);
        bg.graphics.drawRect(0, 0, this.width, LL.Config.personTabBtnHeight * 6);
        bg.graphics.endFill();
        group.addChild(bg);
        var layout = new eui.VerticalLayout();
        layout.gap = 0;
        layout.verticalAlign = egret.VerticalAlign.TOP;
        group.layout = layout;
        //初始化6个按钮
        var hairBtn = new tabBtn("icon_hair_inv.74660_png");
        group.addChild(hairBtn);
        hairBtn.onFocus(function () {
            _this.setFocusState(0);
        });
        this.btnList.push(hairBtn);
        var glassesBtn = new tabBtn("icon_glass_inv.1797a_png");
        group.addChild(glassesBtn);
        this.btnList.push(glassesBtn);
        glassesBtn.onFocus(function () {
            _this.setFocusState(1);
        });
        var beardBtn = new tabBtn("icon_beard_inv.cd11a_png");
        group.addChild(beardBtn);
        this.btnList.push(beardBtn);
        beardBtn.onFocus(function () {
            _this.setFocusState(2);
        });
        var headBtn = new tabBtn("main_face.38433_png");
        group.addChild(headBtn);
        this.btnList.push(headBtn);
        headBtn.onFocus(function () {
            _this.setFocusState(3);
        });
        var bodyBtn = new tabBtn("icon_cloth_inv.9b5f8_png");
        group.addChild(bodyBtn);
        this.btnList.push(bodyBtn);
        bodyBtn.onFocus(function () {
            _this.setFocusState(4);
        });
        var legBtn = new tabBtn("icon_pants_inv.43148_png");
        group.addChild(legBtn);
        this.btnList.push(legBtn);
        legBtn.onFocus(function () {
            _this.setFocusState(5);
        });
        this.viewport = group;
    };
    //设置焦点状态
    personTabBar.prototype.setFocusState = function (index) {
        if (this.focusIndex >= 0) {
            this.btnList[this.focusIndex].setBlur();
        }
        this.focusIndex = index;
        this.btnList[this.focusIndex].setFocus();
        if (this.switch) {
            this.switch(index);
        }
    };
    //绑定变换回调
    personTabBar.prototype.onSwitch = function (callback) {
        this.switch = callback;
    };
    return personTabBar;
}(eui.Scroller));
__reflect(personTabBar.prototype, "personTabBar");
