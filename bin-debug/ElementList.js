/**
 * ★元素列表，可滚动★
 * @param groupName 组名
 * @param height 高度
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var elementList = (function (_super) {
    __extends(elementList, _super);
    function elementList(groupName, width, height, gender) {
        var _this = _super.call(this) || this;
        _this.width = width;
        _this.height = height;
        _this.scrollPolicyH = "ScrollPolicy.OFF";
        _this.visible = false;
        var group = new eui.Group();
        var layout = new eui.TileLayout();
        layout.requestedColumnCount = 4;
        group.width = width;
        group.layout = layout;
        RES.getGroupByName(gender ? gender + groupName : groupName).map(function (item) {
            var itemBox = new elementItem(item.name, groupName, gender);
            group.addChild(itemBox);
        });
        _this.viewport = group;
        return _this;
    }
    elementList.prototype.setState = function (visible) {
        this.visible = visible;
    };
    return elementList;
}(eui.Scroller));
__reflect(elementList.prototype, "elementList");
