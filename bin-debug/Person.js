/**
 * ★显示的人★
 * 人由头、身体、腿和其它元素组成，身体位于容器中心，头和腿的位置由身体决定，各个元素锚点定义在连接处,各个元素可以更换
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var person = (function (_super) {
    __extends(person, _super);
    function person(gender) {
        if (gender === void 0) { gender = "male"; }
        var _this = _super.call(this) || this;
        _this.personConfig = RES.getRes('personConfig_json');
        _this.gender = gender;
        //初始化一个固定尺寸,身体位于这个中心
        _this.width = 400;
        _this.height = 500;
        _this.anchorOffsetX = _this.width / 2;
        _this.anchorOffsetY = _this.height / 2;
        _this.verticalCenter = 0;
        _this.horizontalCenter = 0;
        //头
        _this.createBody(gender === "male" ? "body.06eba_png" : "02.0928d_png");
        _this.createLeg(gender === "male" ? "2.d87a6_png" : "4.07233_png");
        var head = new personHead(gender);
        _this.head = head;
        _this.addChild(head);
        _this.setChildIndex(head, 2);
        _this.calculatePostion();
        //绑定事件
        _this.touchEnabled = true;
        _this.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            LL.GlobalEvents.emitEvents("switchToPerson", gender === "male" ? { index: 4 } : { index: 5 });
        }, _this);
        return _this;
    }
    //创建身体
    person.prototype.createBody = function (bodyName) {
        if (this.body) {
            this.removeChild(this.body);
        }
        this.bodyConfig = this.personConfig['body'][bodyName];
        var bodyData = RES.getRes(bodyName);
        var body = new eui.Image(bodyData);
        body.anchorOffsetX = bodyData.bitmapData.width / 2;
        body.anchorOffsetY = bodyData.bitmapData.height / 2;
        body.x = this.width / 2;
        body.y = this.height / 2;
        this.body = body;
        this.addChild(body);
        this.setChildIndex(body, 1);
    };
    //创建腿
    person.prototype.createLeg = function (legName) {
        if (this.leg) {
            this.removeChild(this.leg);
        }
        var legConfig = this.personConfig['leg'][legName];
        var legData = RES.getRes(legName);
        var leg = new eui.Image(legData);
        leg.anchorOffsetX = legConfig.anchorOffsetX;
        leg.anchorOffsetY = legConfig.anchorOffsetY;
        this.leg = leg;
        this.addChild(leg);
        this.setChildIndex(leg, 0);
    };
    //计算位置
    person.prototype.calculatePostion = function () {
        var source = this.body.source;
        this.head.x = this.width / 2 - source.bitmapData.width / 2 + this.bodyConfig.headX;
        this.head.y = this.height / 2 - source.bitmapData.height / 2 + this.bodyConfig.headY;
        this.leg.x = this.width / 2 - source.bitmapData.width / 2 + this.bodyConfig.legX;
        this.leg.y = this.height / 2 - source.bitmapData.height / 2 + this.bodyConfig.legY;
    };
    //改变装扮
    person.prototype.changePerson = function (e) {
        if (e.groupName === "body") {
            this.createBody(e.name);
        }
        else if (e.groupName === "leg") {
            this.createLeg(e.name);
        }
        else {
            this.head.createElement(e);
        }
        this.calculatePostion();
    };
    return person;
}(eui.Group));
__reflect(person.prototype, "person");
