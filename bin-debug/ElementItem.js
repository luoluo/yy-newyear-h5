var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * ★操作区域元素项★
 * @param name 元素名称
 * @param groupName 组名
 */
var elementItem = (function (_super) {
    __extends(elementItem, _super);
    function elementItem(name, groupName, gender) {
        var _this = _super.call(this) || this;
        _this.width = LL.Config.stageWidth / 4;
        _this.height = 150;
        var imageData = RES.getRes(name);
        var image = new eui.Image(imageData);
        var maxSize = LL.getImageSize(imageData.textureWidth, imageData.textureHeight, _this.width - 40, _this.height - 30);
        image.width = maxSize.width;
        image.height = maxSize.height;
        image.horizontalCenter = 0;
        image.verticalCenter = 0;
        //绑定事件
        _this.touchEnabled = true;
        if (gender) {
            _this.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
                evt.stopImmediatePropagation();
                evt.stopPropagation();
                LL.GlobalEvents.emitEvents("changePerson", {
                    name: name,
                    groupName: groupName
                });
            }, _this);
        }
        else {
            _this.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
                evt.stopImmediatePropagation();
                evt.stopPropagation();
                LL.GlobalEvents.emitEvents("addElement", {
                    name: name,
                    groupName: groupName
                });
            }, _this);
        }
        _this.addChild(image);
        return _this;
    }
    return elementItem;
}(eui.Group));
__reflect(elementItem.prototype, "elementItem");
