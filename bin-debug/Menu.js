/**
 * ★舞台下部操作区域★
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var menu = (function (_super) {
    __extends(menu, _super);
    function menu() {
        var _this = _super.call(this) || this;
        //菜单状态
        _this.state = true;
        //激活的tab序号
        _this.activeIndex = 0;
        //列表元素缓存
        _this.elements = [];
        //计算尺寸
        var w = document.documentElement.clientWidth;
        var h = document.documentElement.clientHeight;
        var height = (h - w * LL.Config.stageDisplayHeight / LL.Config.stageWidth) * 2;
        _this.width = LL.Config.stageWidth;
        _this.height = height;
        _this.mask = new egret.Rectangle(0, 0, _this.width, height);
        _this.x = 0;
        _this.y = LL.Config.stageDisplayHeight;
        _this.drawBg(_this.width, height);
        _this.createTab(_this.width, height - LL.Config.tabHeight);
        return _this;
    }
    // 绘制背景
    menu.prototype.drawBg = function (width, height) {
        var bg = new egret.Shape();
        bg.graphics.beginFill(0xffffff);
        bg.graphics.drawRect(0, 0, width, height);
        bg.graphics.endFill();
        this.addChild(bg);
    };
    //创建内容
    menu.prototype.createTab = function (width, height) {
        var _this = this;
        var TabTar = new tabBar(this.activeIndex);
        this.addChild(TabTar);
        //初始化5个列表
        var bgList = new elementList("background", width, height);
        bgList.x = 0;
        bgList.y = LL.Config.tabHeight;
        this.elements.push(bgList);
        this.addChild(bgList);
        var personList = new elementList("person", width, height);
        personList.x = 0;
        personList.y = LL.Config.tabHeight;
        this.elements.push(personList);
        this.addChild(personList);
        var babyList = new elementList("baby", width, height);
        babyList.x = 0;
        babyList.y = LL.Config.tabHeight;
        this.elements.push(babyList);
        this.addChild(babyList);
        var catList = new elementList("cat", width, height);
        catList.x = 0;
        catList.y = LL.Config.tabHeight;
        this.elements.push(catList);
        this.addChild(catList);
        //男性装扮列表
        var maleList = new personElementBox("male", width, height);
        maleList.x = 0;
        maleList.y = LL.Config.tabHeight;
        this.elements.push(maleList);
        this.addChild(maleList);
        //女性装扮列表
        var femaleList = new personElementBox("female", width, height);
        femaleList.x = 0;
        femaleList.y = LL.Config.tabHeight;
        this.elements.push(femaleList);
        this.addChild(femaleList);
        bgList.setState(true);
        TabTar.onSwitch(function (index) {
            _this.switchTab(index);
        });
        TabTar.onHide(function () {
            if (_this.state) {
                _this.state = false;
                egret.Tween.get(_this).to({ y: LL.Config.stageHeight }, 300, egret.Ease.sineIn);
            }
            else {
                _this.state = true;
                egret.Tween.get(_this).to({ y: LL.Config.stageDisplayHeight }, 300, egret.Ease.sineIn);
            }
        });
        LL.GlobalEvents.addEvents("switchToPerson", function (e) {
            _this.switchTab(e.index);
        });
    };
    //变换tab事件
    menu.prototype.switchTab = function (index) {
        if (this.activeIndex >= 0) {
            this.elements[this.activeIndex].setState(false);
        }
        this.activeIndex = index;
        this.elements[index].setState(true);
    };
    return menu;
}(eui.Group));
__reflect(menu.prototype, "menu");
