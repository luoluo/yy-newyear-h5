var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * ★可移动对象★
 * 点击的时候显示删除按钮，可以移动和缩放
 */
var moveGroup = (function (_super) {
    __extends(moveGroup, _super);
    function moveGroup(e) {
        var _this = _super.call(this) || this;
        //焦点状态
        _this.focusState = false;
        _this.resName = name;
        var Container;
        //针对人类别需要特殊处理
        if (e.groupName === "person") {
            Container = new person(e.name);
            _this.changePerson = function (e) {
                Container.changePerson(e);
            };
        }
        else {
            Container = new container(e.name);
        }
        _this.Container = Container;
        _this.addChild(Container);
        _this.width = Container.width;
        _this.height = Container.height;
        Container.anchorOffsetX = Container.width / 2;
        Container.anchorOffsetY = Container.height / 2;
        _this.anchorOffsetX = _this.width / 2;
        _this.anchorOffsetY = _this.height / 2;
        _this.x = 375;
        _this.y = 400;
        _this.creatDel();
        _this.creatScale();
        //绑定事件
        _this.touchEnabled = true;
        _this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.setFocus, _this);
        return _this;
    }
    // 创建删除按钮
    moveGroup.prototype.creatDel = function () {
        var _this = this;
        var delData = RES.getRes("remove.e80f2_png");
        var del = new egret.Bitmap(delData);
        del.width = 60;
        del.height = 60;
        del.anchorOffsetX = del.width / 2;
        del.anchorOffsetY = del.height / 2;
        del.x = -del.width / 2;
        del.y = -del.height / 2;
        del.touchEnabled = true;
        del.visible = false;
        this.delBtn = del;
        del.addEventListener(egret.TouchEvent.TOUCH_TAP, function (evt) {
            evt.stopImmediatePropagation();
            evt.stopPropagation();
            _this.remove();
        }, this);
        this.addChild(del);
    };
    // 创建缩放按钮
    moveGroup.prototype.creatScale = function () {
        var _this = this;
        var scalleData = RES.getRes("resize.65162_png");
        var scale = new egret.Bitmap(scalleData);
        scale.width = 60;
        scale.height = 60;
        scale.anchorOffsetX = scale.width / 2;
        scale.anchorOffsetY = scale.height / 2;
        scale.x = this.width + scale.width / 2;
        scale.y = -scale.height / 2;
        scale.touchEnabled = true;
        scale.visible = false;
        this.scaleBtn = scale;
        this.addEventListener(egret.Event.RESIZE, function () {
            _this.scaleBtn.x = _this.width + _this.scaleBtn.width / 2;
        }, this);
        scale.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.bindScale, this);
        scale.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.bindScale, this);
        scale.addEventListener(egret.TouchEvent.TOUCH_END, this.bindScale, this);
        this.addChild(scale);
    };
    // 绑定点击事件
    moveGroup.prototype.onTap = function (callBack) {
        this.tapEvent = callBack;
    };
    // 绑定删除事件
    moveGroup.prototype.onDelTap = function (callBack) {
        this.remove = callBack;
    };
    moveGroup.prototype.touchEvent = function (evt) {
        evt.stopImmediatePropagation();
        evt.stopPropagation();
        switch (evt.type) {
            case egret.TouchEvent.TOUCH_MOVE:
                if (this.initX && this.initY) {
                    this.x += evt.stageX - this.initX;
                    this.y += evt.stageY - this.initY;
                    this.initX = evt.stageX;
                    this.initY = evt.stageY;
                }
                else {
                    this.initX = evt.stageX;
                    this.initY = evt.stageY;
                }
                break;
            case egret.TouchEvent.TOUCH_BEGIN:
                this.initX = evt.stageX;
                this.initY = evt.stageY;
                this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchEvent, this);
                this.once(egret.TouchEvent.TOUCH_END, this.touchEvent, this);
                break;
            case egret.TouchEvent.TOUCH_END:
                this.initX = 0;
                this.initY = 0;
                this.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchEvent, this);
                break;
        }
    };
    moveGroup.prototype.bindScale = function (evt) {
        evt.stopImmediatePropagation();
        evt.stopPropagation();
        switch (evt.type) {
            case egret.TouchEvent.TOUCH_BEGIN:
                this.initLocalX = evt.localX;
                this.initLocalY = evt.localY;
                this.initAngle = this.getTouchAngle(this.width / 2 + evt.localX, this.height / 2 + 60 - evt.localY);
                this.initDistance = this.getTouchDistance(this.width / 2, this.height / 2);
                this.initScale = this.Container.scaleX;
                break;
            case egret.TouchEvent.TOUCH_MOVE:
                var newX = evt.localX - this.initLocalX + this.width / 2;
                var newY = this.initLocalY - evt.localY + this.height / 2;
                var newDistance = this.getTouchDistance(newX, newY);
                var newAngele = this.getTouchAngle(this.width / 2 + evt.localX, this.height / 2 + 60 - evt.localY);
                if (!this.initDistance || !this.initAngle) {
                    this.initDistance = newDistance;
                    this.initAngle = newAngele;
                    this.initScale = this.Container.scaleX;
                    return;
                }
                var scaleNum = newDistance / this.initDistance;
                this.Container.scaleX = scaleNum * this.initScale;
                this.Container.scaleY = scaleNum * this.initScale;
                this.width = this.Container.width * this.Container.scaleX;
                this.height = this.Container.height * this.Container.scaleY;
                this.anchorOffsetX = this.width / 2;
                this.anchorOffsetY = this.height / 2;
                var angle = this.initAngle - newAngele;
                if (angle < 15 && angle > -15) {
                    this.rotation += angle;
                }
                break;
            case egret.TouchEvent.TOUCH_END:
                this.initLocalX = 0;
                this.initLocalY = 0;
                this.initDistance = 0;
                break;
        }
    };
    //设置焦点状态
    moveGroup.prototype.setFocus = function (evt) {
        evt.stopImmediatePropagation();
        evt.stopPropagation();
        this.focusState = true;
        this.delBtn.visible = true;
        this.scaleBtn.visible = true;
        this.tapEvent();
        this.touchEvent(evt);
    };
    //取消焦点状态
    moveGroup.prototype.setblur = function () {
        this.focusState = false;
        this.delBtn.visible = false;
        this.scaleBtn.visible = false;
        this.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchEvent, this);
    };
    //通过坐标返回角度
    moveGroup.prototype.getTouchAngle = function (x, y) {
        var c = 0.017453292; //2PI/360
        var res = Math.atan2(y, x) / c;
        if (x > 0 && y > 0) {
            return res;
        }
        else if (x < 0 && y > 0) {
            return res + 180;
        }
        else if (x < 0 && y < 0) {
            return res + 360;
        }
        else {
            return res + 180;
        }
    };
    //通过坐标返回距离
    moveGroup.prototype.getTouchDistance = function (x, y) {
        return Math.sqrt(x * x + y * y);
    };
    return moveGroup;
}(eui.Group));
__reflect(moveGroup.prototype, "moveGroup");
