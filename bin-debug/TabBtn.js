/**
 * ★舞台下部操作tab按钮★
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var tabBtn = (function (_super) {
    __extends(tabBtn, _super);
    function tabBtn(name) {
        var _this = _super.call(this) || this;
        _this.width = LL.Config.tabWidth;
        _this.height = LL.Config.tabHeight;
        _this.drawBg();
        _this.drawImg(name);
        _this.touchEnabled = true;
        _this.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            if (_this.focus) {
                _this.focus();
            }
        }, _this);
        return _this;
    }
    //绘制背景
    tabBtn.prototype.drawBg = function () {
        var bg = new egret.Shape();
        bg.graphics.beginFill(0xffffff, 1);
        bg.graphics.drawRect(0, 0, this.width, this.height);
        bg.graphics.endFill();
        bg.visible = false;
        this.bg = bg;
        this.addChild(bg);
    };
    //绘制图片
    tabBtn.prototype.drawImg = function (name) {
        var imgData = RES.getRes(name);
        var img = new eui.Image(imgData);
        var maxSize = LL.getImageSize(imgData.textureWidth, imgData.textureHeight, LL.Config.tabWidth - 20, LL.Config.tabHeight - 20);
        img.width = maxSize.width;
        img.height = maxSize.height;
        img.anchorOffsetX = this.width / 2;
        img.anchorOffsetY = this.height / 2;
        img.horizontalCenter = 0;
        img.verticalCenter = 0;
        this.addChild(img);
    };
    //设置焦点状态
    tabBtn.prototype.setFocus = function () {
        this.bg.visible = true;
    };
    //设置失焦状态
    tabBtn.prototype.setBlur = function () {
        this.bg.visible = false;
    };
    //绑定焦点事件
    tabBtn.prototype.onFocus = function (callback) {
        this.focus = callback;
    };
    return tabBtn;
}(eui.Group));
__reflect(tabBtn.prototype, "tabBtn");
