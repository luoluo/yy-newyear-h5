/**
 * ★人的头部★
 * 头部由各种头部元素构成，处理机制与人一致。由头部，头发，眼镜，胡须等组成
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var personHead = (function (_super) {
    __extends(personHead, _super);
    function personHead(gender) {
        if (gender === void 0) { gender = "male"; }
        var _this = _super.call(this) || this;
        //创建头部
        _this.personConfig = RES.getRes('personConfig_json');
        var headName = gender === "male" ? "main_face.7692e_png" : "main_face.38433_png";
        var headConfig = _this.personConfig['head'][headName];
        _this.headConfig = headConfig;
        var headData = RES.getRes(headName);
        var head = new eui.Image(headData);
        head.anchorOffsetX = headConfig.anchorOffsetX;
        head.anchorOffsetY = headConfig.anchorOffsetY;
        head.x = headConfig.anchorOffsetX;
        head.y = headConfig.anchorOffsetY;
        _this.addChild(head);
        //初始化一个尺寸,这个尺寸等同于头部尺寸
        _this.width = headData.bitmapData.width;
        _this.height = headData.bitmapData.height;
        _this.anchorOffsetX = headConfig.anchorOffsetX;
        _this.anchorOffsetY = headConfig.anchorOffsetY;
        return _this;
    }
    //创建一个头部元素
    personHead.prototype.createElement = function (e) {
        if (this[e.groupName]) {
            this.removeChild(this[e.groupName]);
        }
        var config = this.personConfig[e.groupName][e.name];
        var data = RES.getRes(e.name);
        var element = new eui.Image(data);
        element.anchorOffsetX = config.anchorOffsetX;
        element.anchorOffsetY = config.anchorOffsetY;
        this[e.groupName] = element;
        this.addChild(element);
        element.x = this.headConfig[e.groupName + "X"];
        element.y = this.headConfig[e.groupName + "Y"];
        if (this.hair) {
            this.setChildIndex(this.hair, this.numChildren - 1);
        }
    };
    return personHead;
}(eui.Group));
__reflect(personHead.prototype, "personHead");
