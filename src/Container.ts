
/**
 * ★内容对象★
 * 内容是一个图片，图片居中，容器宽高与图片相同
 * @param name 图片名称
 */
class container extends eui.Group{
  constructor(name:string){
    super();

    let imageData: egret.Texture = RES.getRes(name);
    let image: egret.Bitmap = new egret.Bitmap(imageData);
    this.width = image.width = imageData.textureWidth / 2;
    this.height = image.height = imageData.textureHeight / 2;
    this.anchorOffsetX = this.width / 2;
    this.anchorOffsetY = this.height / 2;
    this.horizontalCenter=0;
    this.verticalCenter=0;
    this.addChild(image);
  }
}