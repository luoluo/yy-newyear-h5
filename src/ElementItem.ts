/**
 * ★操作区域元素项★
 * @param name 元素名称
 * @param groupName 组名
 */
class elementItem extends eui.Group{
  constructor(name:string,groupName:string,gender?){
    super()

    this.width=LL.Config.stageWidth/4;
    this.height=150;

    let imageData=RES.getRes(name)
    let image = new eui.Image(imageData)
    let maxSize=LL.getImageSize(imageData.textureWidth,imageData.textureHeight,this.width-40,this.height-30);
    image.width=maxSize.width
    image.height=maxSize.height
    image.horizontalCenter=0;
    image.verticalCenter=0;
    //绑定事件
    this.touchEnabled=true;
    if(gender){
      this.addEventListener(egret.TouchEvent.TOUCH_TAP,(evt:egret.TouchEvent)=>{
        evt.stopImmediatePropagation();
        evt.stopPropagation();
        LL.GlobalEvents.emitEvents("changePerson",{
          name:name,
          groupName:groupName
        })
      },this)
    }else{
      this.addEventListener(egret.TouchEvent.TOUCH_TAP,(evt:egret.TouchEvent)=>{
        evt.stopImmediatePropagation();
        evt.stopPropagation();
        LL.GlobalEvents.emitEvents("addElement",{
          name:name,
          groupName:groupName
        })
      },this)
    }
    this.addChild(image)
  }
}