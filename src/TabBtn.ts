/**
 * ★舞台下部操作tab按钮★
 */

class tabBtn extends eui.Group{
  //背景
  private bg:egret.Shape;
  //焦点回调
  private focus:Function;

  constructor(name:string){
    super();
    this.width=LL.Config.tabWidth;
    this.height=LL.Config.tabHeight;
    this.drawBg()
    this.drawImg(name)
    this.touchEnabled=true;
    this.addEventListener(egret.TouchEvent.TOUCH_TAP,()=>{
      if(this.focus){
        this.focus()
      }
    },this)
  }
  //绘制背景
  private drawBg():void{
    var bg=new egret.Shape();
    bg.graphics.beginFill(0xffffff,1);
    bg.graphics.drawRect(0,0,this.width,this.height);
    bg.graphics.endFill();
    bg.visible=false;
    this.bg=bg;
    this.addChild(bg)
  }
  //绘制图片
  public drawImg(name:string):void{
    var imgData: egret.Texture = RES.getRes(name);
    var img=new eui.Image(imgData)
    var maxSize=LL.getImageSize(imgData.textureWidth,imgData.textureHeight,LL.Config.tabWidth-20,LL.Config.tabHeight-20);
    img.width=maxSize.width;
    img.height=maxSize.height;
    img.anchorOffsetX=this.width/2;
    img.anchorOffsetY=this.height/2;
    img.horizontalCenter=0;
    img.verticalCenter=0
    this.addChild(img)
  }
  //设置焦点状态
  public setFocus():void{
    this.bg.visible=true;
  }
  //设置失焦状态
  public setBlur():void{
    this.bg.visible=false;
  }
  //绑定焦点事件
  public onFocus(callback:Function){
    this.focus=callback
  }
}