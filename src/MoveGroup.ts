
/**
 * ★可移动对象★
 * 点击的时候显示删除按钮，可以移动和缩放
 */
class moveGroup extends eui.Group {
  //移除事件
  private remove: Function;
  //点击事件
  private tapEvent: Function;
  //资源名字
  private resName: string;
  //焦点状态
  private focusState:boolean=false;
  //内容组
  private Container:eui.Group;
  //删除按钮
  private delBtn:egret.Bitmap;
  //放缩及移动按钮
  private scaleBtn:egret.Bitmap;
  //改变装扮
  public changePerson:Function


  constructor(e) {
    super();

    this.resName=name;
    let Container
    //针对人类别需要特殊处理
    if(e.groupName==="person"){
      Container=new person(e.name)
      this.changePerson=(e)=>{
        Container.changePerson(e)
      }
    }else{
      Container=new container(e.name)
    }
    this.Container=Container
    this.addChild(Container)
    this.width=Container.width;
    this.height=Container.height;
    Container.anchorOffsetX=Container.width/2;
    Container.anchorOffsetY=Container.height/2;
    this.anchorOffsetX=this.width/2;
    this.anchorOffsetY=this.height/2;
    this.x = 375;
    this.y = 400;

    this.creatDel();
    this.creatScale();
    //绑定事件
    this.touchEnabled=true
    this.addEventListener(egret.TouchEvent.TOUCH_BEGIN,this.setFocus,this)
  }

  // 创建删除按钮
  private creatDel(): void {
    var delData: egret.Texture = RES.getRes("remove.e80f2_png");
    var del: egret.Bitmap = new egret.Bitmap(delData);
    del.width = 60;
    del.height = 60;
    del.anchorOffsetX=del.width/2;
    del.anchorOffsetY=del.height/2;
    del.x = -del.width/2;
    del.y = -del.height/2;
    del.touchEnabled = true;
    del.visible=false;
    this.delBtn=del;
    del.addEventListener(egret.TouchEvent.TOUCH_TAP,(evt) => {
      evt.stopImmediatePropagation();
      evt.stopPropagation()
      this.remove();
    },this);
    this.addChild(del);
  }

  // 创建缩放按钮
  private creatScale():void{
    var scalleData: egret.Texture = RES.getRes("resize.65162_png");
    var scale: egret.Bitmap = new egret.Bitmap(scalleData);
    scale.width = 60;
    scale.height = 60;
    scale.anchorOffsetX=scale.width/2;
    scale.anchorOffsetY=scale.height/2;
    scale.x = this.width+scale.width/2;
    scale.y = -scale.height/2;
    scale.touchEnabled = true;
    scale.visible=false;
    this.scaleBtn=scale;
    this.addEventListener(egret.Event.RESIZE,()=>{
      this.scaleBtn.x=this.width+this.scaleBtn.width/2;
    },this)
    scale.addEventListener(egret.TouchEvent.TOUCH_BEGIN,this.bindScale,this);
    scale.addEventListener(egret.TouchEvent.TOUCH_MOVE,this.bindScale,this);
    scale.addEventListener(egret.TouchEvent.TOUCH_END,this.bindScale,this);
    this.addChild(scale);
  }
   
  // 绑定点击事件
  public onTap(callBack: Function){
    this.tapEvent=callBack
  }
  // 绑定删除事件
  public onDelTap(callBack: Function): void {
    this.remove = callBack;
  }

  // 移动事件
  private initX:number;
  private initY:number;

  private touchEvent(evt: egret.TouchEvent){
    evt.stopImmediatePropagation();
    evt.stopPropagation();
    switch (evt.type) {
      case egret.TouchEvent.TOUCH_MOVE:
        if(this.initX&&this.initY){
          this.x+=evt.stageX-this.initX;
          this.y+=evt.stageY-this.initY;
          this.initX=evt.stageX;
          this.initY=evt.stageY;
        }else{
          this.initX=evt.stageX;
          this.initY=evt.stageY;
        }
        break;
      case egret.TouchEvent.TOUCH_BEGIN:
        this.initX=evt.stageX;
        this.initY=evt.stageY;
        this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchEvent, this);
        this.once(egret.TouchEvent.TOUCH_END, this.touchEvent, this);
        break;
      case egret.TouchEvent.TOUCH_END:
        this.initX=0;
        this.initY=0;
        this.removeEventListener(egret.TouchEvent.TOUCH_MOVE,this.touchEvent,this);
        break;
    }
  }

  // 缩放及旋转事件
  private initScale:number;
  private initAngle:number;
  private initDistance:number;

  private initLocalX:number;
  private initLocalY:number;
  
  private bindScale(evt:egret.TouchEvent){
    evt.stopImmediatePropagation();
    evt.stopPropagation()
    switch (evt.type){
      case egret.TouchEvent.TOUCH_BEGIN:
        this.initLocalX=evt.localX;
        this.initLocalY=evt.localY;
        this.initAngle=this.getTouchAngle(this.width/2+evt.localX,this.height/2+60-evt.localY)
        this.initDistance=this.getTouchDistance(this.width/2,this.height/2)
        this.initScale=this.Container.scaleX;
      break;
      case egret.TouchEvent.TOUCH_MOVE:
        let newX=evt.localX-this.initLocalX+this.width/2
        let newY=this.initLocalY-evt.localY+this.height/2
        let newDistance=this.getTouchDistance(newX,newY)
        let newAngele=this.getTouchAngle(this.width/2+evt.localX,this.height/2+60-evt.localY)
        if(!this.initDistance||!this.initAngle){
          this.initDistance=newDistance
          this.initAngle=newAngele
          this.initScale=this.Container.scaleX
          return
        }
        let scaleNum=newDistance/this.initDistance
        this.Container.scaleX=scaleNum*this.initScale;
        this.Container.scaleY=scaleNum*this.initScale;
        this.width=this.Container.width*this.Container.scaleX
        this.height=this.Container.height*this.Container.scaleY
        this.anchorOffsetX=this.width/2;
        this.anchorOffsetY=this.height/2;
        let angle = this.initAngle-newAngele
        if(angle<15&&angle>-15){
          this.rotation+=angle
        }
      break;
      case egret.TouchEvent.TOUCH_END:
        this.initLocalX=0;
        this.initLocalY=0;
        this.initDistance=0;
      break;
    }
  }

  //设置焦点状态
  public setFocus(evt){
    evt.stopImmediatePropagation();
    evt.stopPropagation();
    this.focusState=true;
    this.delBtn.visible=true;
    this.scaleBtn.visible=true;
    this.tapEvent();
    this.touchEvent(evt)
  }

  //取消焦点状态
  public setblur(){
    this.focusState=false;
    this.delBtn.visible=false;
    this.scaleBtn.visible=false;
    this.removeEventListener(egret.TouchEvent.TOUCH_MOVE,this.touchEvent,this)
  }

  //通过坐标返回角度
  private getTouchAngle(x:number,y:number):number{
    const c:number = 0.017453292; //2PI/360
    const res=Math.atan2(y,x) / c;
    if(x>0&&y>0){
      return res
    }else if(x<0&&y>0){
      return res+180
    }else if(x<0&&y<0) {
      return res+360
    }else{
      return res+180
    }
  }
  //通过坐标返回距离
  private getTouchDistance(x:number,y:number):number{
    return Math.sqrt(x*x+y*y)
  }
}
