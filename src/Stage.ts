/**
 * ★H5主要舞台区域场景★
 * 绘制各种元素的区域
 */

interface elementParameter{
  name:string,
  groupName:string
}

class stage extends eui.Group{
  //缓存截图
  public photoImg:egret.RenderTexture;

  constructor(){
    super()

    this.width=LL.Config.stageWidth;
    this.height=LL.Config.stageHeight;
    this.mask=new egret.Rectangle(0,0,this.width,this.height);

    this.drawBg()
    this.drawCamera()
    //绑定事件
    this.addEventListener(egret.TouchEvent.TOUCH_TAP,(evt)=>{
      if(this.currentItem&&evt.target===this){
        this.currentItem.setblur()
      }
    },this)
    LL.GlobalEvents.addEvents("addElement",(e)=>{
      this.addElement(e)
    })
    LL.GlobalEvents.addEvents("changePerson",(e)=>{
      if(this.currentItem&&this.currentItem.changePerson){
        this.currentItem.changePerson(e)
      }
    })
  }
  // 背景
  private drawBg():void{
    var bgData:egret.Texture=RES.getRes("Room.adb50_png");
    var bg:egret.Bitmap=new egret.Bitmap(bgData);
    bg.x=0;
    bg.y=0;
    bg.width=this.width;
    bg.height=this.height;
    this.addChild(bg)
  }

  // 相机
  private drawCamera():void{
    var cameraData:egret.Texture=RES.getRes("icon_camera.8e92f_png");
    var camera:egret.Bitmap=new egret.Bitmap(cameraData);
    camera.x=this.width-90;
    camera.y=LL.Config.stageDisplayHeight-90;
    camera.width=80;
    camera.height=80;
    camera.touchEnabled=true;
    camera.addEventListener(egret.TouchEvent.TOUCH_TAP,(evt)=>{
      evt.stopImmediatePropagation();
      evt.stopPropagation()
      camera.visible=false 
      var screenShot:egret.RenderTexture=new egret.RenderTexture;
      screenShot.drawToTexture(this)
      this.photoImg=screenShot
      camera.visible=true
    },this)
    this.addChild(camera)
  }

  // 添加元素
  public addElement(e:elementParameter){
    let item = new moveGroup(e)
    item.onDelTap(()=>{
      this.removeChild(item)
    })
    item.onTap(()=>{
      if(this.currentItem&&this.currentItem!=item){
        this.currentItem.setblur()
      }
      if(e.groupName!=="background"){
        this.setChildIndex(item,this.numChildren-2)
      }
      this.currentItem=item
    })
    this.addChild(item)
    this.setChildIndex(item,this.numChildren-2)
  }

  // 当前焦点元素
  private currentItem:moveGroup;

}

