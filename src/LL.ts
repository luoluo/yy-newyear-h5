/***********************
======▶项目库文件◀======
========★落落★========
***********************/

interface size{
  width:number,
  height:number
}
//====★全局事件★====//
class globalEvents {
  private Events:object={};

  //添加事件
  public addEvents(name:string,callback:Function){
    if(!this.Events[name]){
      this.Events[name]=[]
    }
    this.Events[name].push(callback)
  }
  //触发事件
  public emitEvents(name:string,parameter={}){
    if(!this.Events[name]){
      console.error(name+"事件未注册")
      return
    }
    this.Events[name].map((callback)=>{
      callback(parameter)
    })
  }
  //删除事件
  public removeEvents(name:string){
    if(this.Events[name]){
      delete this.Events[name]
    }
  }
}


const LL = {
  //====★根据父元素尺寸计算图片宽高★====//
  getImageSize:function(width:number,height:number,maxWidth:number,maxHeight:number,mode:string="scale"):size{
    switch (mode){
      case "scale":
      default:
      const ratio=width/height
      const maxRatio=maxWidth/maxHeight
      if(ratio>maxRatio){
        return {
          width:maxWidth,
          height:height*maxWidth/width
        }
      }else{
        return {
          width:width*maxHeight/height,
          height:maxHeight
        }
      }
    }
  },
  //====★全局事件★====//
  GlobalEvents:new globalEvents(),
  //全局配置
  Config:{
    stageWidth:750,//舞台宽
    stageHeight:974,//舞台高
    stageDisplayHeight:800,//舞台显示高
    tabWidth:96,//tab按钮宽
    tabHeight:80,//tab按钮高,
    personTabBtnWidth:96,//人tab按钮高
    personTabBtnHeight:96,//人tab按钮宽
  }
}