/**
 * ★舞台下部操作tab操作区域★
 */

class personTabBar extends eui.Scroller{
  //焦点序号
  private focusIndex:number=0;
  //按钮对象组
  private btnList:Array<tabBtn>=[];
  //变换回调
  private switch:Function;

  constructor(height:number){
    super()

    this.width=LL.Config.personTabBtnWidth;
    this.scrollPolicyH="ScrollPolicy.OFF"
    this.createTabBtn(height);
    this.setFocusState(this.focusIndex)
  }

  //创建6个按钮
  private createTabBtn(height:number):void{
    //布局
    let group = new eui.Group();
    group.height=height

    //背景
    var bg=new egret.Shape()
    bg.graphics.beginFill(0xfafafa,1);
    bg.graphics.drawRect(0,0,this.width,LL.Config.personTabBtnHeight*6);
    bg.graphics.endFill();
    group.addChild(bg);

    let layout=new eui.VerticalLayout()
    layout.gap=0;
    layout.verticalAlign=egret.VerticalAlign.TOP
    group.layout=layout
    //初始化6个按钮
    var hairBtn=new tabBtn("icon_hair_inv.74660_png");
    group.addChild(hairBtn)
    hairBtn.onFocus(()=>{
      this.setFocusState(0)
    })
    this.btnList.push(hairBtn)
    
    var glassesBtn=new tabBtn("icon_glass_inv.1797a_png");
    group.addChild(glassesBtn)
    this.btnList.push(glassesBtn)
    glassesBtn.onFocus(()=>{
      this.setFocusState(1)
    })
    
    var beardBtn=new tabBtn("icon_beard_inv.cd11a_png");
    group.addChild(beardBtn)
    this.btnList.push(beardBtn)
    beardBtn.onFocus(()=>{
      this.setFocusState(2)
    })

    var headBtn=new tabBtn("main_face.38433_png");
    group.addChild(headBtn)
    this.btnList.push(headBtn)
    headBtn.onFocus(()=>{
      this.setFocusState(3)
    })

    var bodyBtn=new tabBtn("icon_cloth_inv.9b5f8_png");
    group.addChild(bodyBtn)
    this.btnList.push(bodyBtn)
    bodyBtn.onFocus(()=>{
      this.setFocusState(4)
    })

    var legBtn=new tabBtn("icon_pants_inv.43148_png");
    group.addChild(legBtn)
    this.btnList.push(legBtn)
    legBtn.onFocus(()=>{
      this.setFocusState(5)
    })
    this.viewport=group
  }
  //设置焦点状态
  private setFocusState(index:number){
    if(this.focusIndex>=0){
      this.btnList[this.focusIndex].setBlur()
    }
    this.focusIndex=index
    this.btnList[this.focusIndex].setFocus()
    if(this.switch){
      this.switch(index)
    }
  }
  //绑定变换回调
  public onSwitch(callback:Function){
    this.switch=callback
  }
}