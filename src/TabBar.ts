/**
 * ★舞台下部操作tab操作区域★
 */

class tabBar extends eui.Group{
  //焦点序号
  private focusIndex:number;
  //按钮对象组
  private btnList:Array<tabBtn>=[];
  //变换回调
  private switch:Function;
  //隐藏回调
  private hide:Function;

  constructor(index:number=0){
    super()

    this.width=LL.Config.stageWidth;
    this.height=LL.Config.tabHeight;
    this.top=0;
    this.left=0;
    //背景
    var bg=new egret.Shape()
    bg.graphics.beginFill(0xf5f5f5,1);
    bg.graphics.drawRect(0,0,this.width,this.height);
    bg.graphics.endFill();
    this.addChild(bg);

    this.createTabBtn();
    this.createFold()
    this.focusIndex=index;
    this.setFocusState(index)
  }

  //创建5个按钮
  private createTabBtn():void{
    var bgBtn=new tabBtn("icon_sofa.fac75_png");
    this.addChild(bgBtn)
    bgBtn.onFocus(()=>{
      this.setFocusState(0)
    })
    this.btnList.push(bgBtn)
    
    var personBtn=new tabBtn("icon_adult.f458f_png");
    this.addChild(personBtn)
    this.btnList.push(personBtn)
    personBtn.onFocus(()=>{
      this.setFocusState(1)
    })
    
    var babyBtn=new tabBtn("icon_kid.c28dd_png");
    this.addChild(babyBtn)
    this.btnList.push(babyBtn)
    babyBtn.onFocus(()=>{
      this.setFocusState(2)
    })

    var catBtn=new tabBtn("icon_cat.d56f8_png");
    this.addChild(catBtn)
    this.btnList.push(catBtn)
    catBtn.onFocus(()=>{
      this.setFocusState(3)
    })
    
    //布局
    var layout:eui.HorizontalLayout=new eui.HorizontalLayout();
    layout.gap=0;
    layout.horizontalAlign=egret.HorizontalAlign.LEFT;
    this.layout=layout
    
  }
  //设置焦点状态
  private setFocusState(index:number){
    if(this.focusIndex>=0){
      this.btnList[this.focusIndex].setBlur()
    }
    this.focusIndex=index
    this.btnList[this.focusIndex].setFocus()
    if(this.switch){
      this.switch(index)
    }
  }
  //绑定变换回调
  public onSwitch(callback:Function){
    this.switch=callback
  }

  //折叠按钮
  private createFold(){
    var fold=new tabBtn("mini.83784_png");
    fold.includeInLayout=false;
    fold.anchorOffsetX=fold.width/2;
    fold.anchorOffsetY=fold.height/2;
    fold.x=LL.Config.stageWidth-fold.anchorOffsetX;
    fold.y=fold.anchorOffsetY;
    fold.touchEnabled=true
    fold.addEventListener(egret.TouchEvent.TOUCH_TAP,(e)=>{
      fold.rotation+=180
      this.hide()
    },this)
    this.addChild(fold)
  }
  public onHide(callback:Function){
    this.hide=callback
  }
}