/**
 * ★舞台下部操作区域★
 */

class menu extends eui.Group{
  //菜单状态
  private state:boolean=true;
  //激活的tab序号
  private activeIndex:number=0;
  //列表元素缓存
  private elements=[];

  constructor (){
    super();

    //计算尺寸
    let w = document.documentElement.clientWidth;
    let h = document.documentElement.clientHeight;
    let height=(h-w*LL.Config.stageDisplayHeight/LL.Config.stageWidth)*2;
    this.width=LL.Config.stageWidth;
    this.height=height
    this.mask=new egret.Rectangle(0,0,this.width,height);
    this.x=0;
    this.y=LL.Config.stageDisplayHeight
    
    this.drawBg(this.width,height)
    this.createTab(this.width,height-LL.Config.tabHeight)
  }

  // 绘制背景
  public drawBg(width:number,height:number){
    var bg=new egret.Shape();
    bg.graphics.beginFill(0xffffff);
    bg.graphics.drawRect(0,0,width,height)
    bg.graphics.endFill();
    this.addChild(bg);
  }
  //创建内容
  private createTab(width:number,height:number){
    var TabTar=new tabBar(this.activeIndex);
    this.addChild(TabTar);
    
    //初始化5个列表
    var bgList = new elementList("background", width,height)
    bgList.x=0
    bgList.y=LL.Config.tabHeight
    this.elements.push(bgList)
    this.addChild(bgList)

    var personList = new elementList("person",width,height)
    personList.x=0
    personList.y=LL.Config.tabHeight
    this.elements.push(personList)
    this.addChild(personList)

    var babyList = new elementList("baby",width,height)
    babyList.x=0
    babyList.y=LL.Config.tabHeight
    this.elements.push(babyList)
    this.addChild(babyList)

    var catList = new elementList("cat",width,height)
    catList.x=0
    catList.y=LL.Config.tabHeight
    this.elements.push(catList)
    this.addChild(catList)
    //男性装扮列表
    var maleList = new personElementBox("male",width,height)
    maleList.x=0
    maleList.y=LL.Config.tabHeight
    this.elements.push(maleList)
    this.addChild(maleList)
    //女性装扮列表
    var femaleList = new personElementBox("female",width,height)
    femaleList.x=0
    femaleList.y=LL.Config.tabHeight
    this.elements.push(femaleList)
    this.addChild(femaleList)

    bgList.setState(true);

    TabTar.onSwitch((index)=>{
      this.switchTab(index)
    })
    TabTar.onHide(()=>{
      if(this.state){
        this.state=false
        egret.Tween.get( this ).to({y:LL.Config.stageHeight}, 300, egret.Ease.sineIn );
      }else{
        this.state=true
        egret.Tween.get( this ).to({y:LL.Config.stageDisplayHeight}, 300, egret.Ease.sineIn );
      }
    })
    LL.GlobalEvents.addEvents("switchToPerson",(e)=>{
      this.switchTab(e.index)
    })
  }
  //变换tab事件
  private switchTab(index){
    if(this.activeIndex>=0){
      this.elements[this.activeIndex].setState(false)
    }
    this.activeIndex=index
    this.elements[index].setState(true)
  }
}
