/**
 * ★人的头部★
 * 头部由各种头部元素构成，处理机制与人一致。由头部，头发，眼镜，胡须等组成
 */

class personHead extends eui.Group{
  //头部
  private head:eui.Image;
  //发型
  private hair:eui.Image;
  //眼镜
  private glasses:eui.Image;
  //胡须
  private beard:eui.Image;
  //配置文件
  private personConfig;
  private headConfig;

  constructor(gender:string="male"){
    super()
    //创建头部
    this.personConfig=RES.getRes('personConfig_json')
    var headName=gender==="male"?"main_face.7692e_png":"main_face.38433_png"
    let headConfig=this.personConfig['head'][headName]
    this.headConfig=headConfig
    let headData:egret.Texture=RES.getRes(headName);
    let head:eui.Image=new eui.Image(headData);
    head.anchorOffsetX=headConfig.anchorOffsetX;
    head.anchorOffsetY=headConfig.anchorOffsetY;
    head.x=headConfig.anchorOffsetX;
    head.y=headConfig.anchorOffsetY;
    this.addChild(head)
    //初始化一个尺寸,这个尺寸等同于头部尺寸
    this.width=headData.bitmapData.width;
    this.height=headData.bitmapData.height;
    this.anchorOffsetX=headConfig.anchorOffsetX;
    this.anchorOffsetY=headConfig.anchorOffsetY;
  }
  //创建一个头部元素
  public createElement(e){
    if(this[e.groupName]){
      this.removeChild(this[e.groupName])
    }
    let config=this.personConfig[e.groupName][e.name]
    let data:egret.Texture=RES.getRes(e.name);
    let element:eui.Image=new eui.Image(data);
    element.anchorOffsetX=config.anchorOffsetX;
    element.anchorOffsetY=config.anchorOffsetY;
    this[e.groupName]=element
    this.addChild(element)
    element.x=this.headConfig[e.groupName+"X"];
    element.y=this.headConfig[e.groupName+"Y"];
    if(this.hair){
      this.setChildIndex(this.hair,this.numChildren-1)
    }
  }
}
