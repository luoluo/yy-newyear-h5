/**
 * ★人元素列表★
 * @param sex 性别
 */

 class personElementBox extends eui.Group{
   //激活的tab序号
  private activeIndex:number=0;
  //列表元素缓存
  private elements=[];

  constructor(gender,width,height){
    super()
    this.width=width;
    this.height=height;
    this.visible=false;
    this.x=0;
    this.y=LL.Config.tabHeight
    this.createTab(gender,LL.Config.stageWidth-LL.Config.personTabBtnWidth,height)
  }
  private createTab(gender,width:number,height:number){
    var TabTar=new personTabBar(height);
    this.addChild(TabTar);
    //初始化5个列表
    var hairList = new elementList("hair", width,height,gender)
    hairList.x=LL.Config.personTabBtnWidth
    hairList.y=0
    this.elements.push(hairList)
    this.addChild(hairList)

    var glassesList = new elementList("glasses",width,height,gender)
    glassesList.x=LL.Config.personTabBtnWidth,
    glassesList.y=0
    this.elements.push(glassesList)
    this.addChild(glassesList)

    var beardList = new elementList("beard",width,height,gender)
    beardList.x=LL.Config.personTabBtnWidth
    beardList.y=0
    this.elements.push(beardList)
    this.addChild(beardList)

    var headList = new elementList("head", width,height,gender)
    headList.x=LL.Config.personTabBtnWidth
    headList.y=0
    this.elements.push(headList)
    this.addChild(headList)

    var bodyList = new elementList("body",width,height,gender)
    bodyList.x=LL.Config.personTabBtnWidth
    bodyList.y=0
    this.elements.push(bodyList)
    this.addChild(bodyList)

    var legList = new elementList("leg",width,height,gender)
    legList.x=LL.Config.personTabBtnWidth
    legList.y=0
    this.elements.push(legList)
    this.addChild(legList)
    
    TabTar.onSwitch((index)=>{
      this.switchTab(index)
    })

    hairList.setState(true)
  }

  //变换tab事件
  private switchTab(index){
    if(this.activeIndex>=0){
      this.elements[this.activeIndex].setState(false)
    }
    this.activeIndex=index
    this.elements[index].setState(true)
  }

  public setState(visible:boolean):void{
    this.visible=visible
  }

}
