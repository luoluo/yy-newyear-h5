/**
 * ★显示的人★
 * 人由头、身体、腿和其它元素组成，身体位于容器中心，头和腿的位置由身体决定，各个元素锚点定义在连接处,各个元素可以更换
 */

class person extends eui.Group{
  //性别
  private gender:string;
  //头
  private head:personHead;
  //身体
  private body:eui.Image;
  //腿
  private leg:eui.Image;
  //缓存配置文件
  private personConfig;
  private bodyConfig;

  constructor(gender:string="male"){
    super()

    this.personConfig=RES.getRes('personConfig_json')
    this.gender=gender;
    //初始化一个固定尺寸,身体位于这个中心
    this.width=400;
    this.height=500;
    this.anchorOffsetX = this.width / 2;
    this.anchorOffsetY = this.height / 2;
    this.verticalCenter=0;
    this.horizontalCenter=0;
    //头
    this.createBody(gender==="male"?"body.06eba_png":"02.0928d_png")
    this.createLeg(gender==="male"?"2.d87a6_png":"4.07233_png")
    let head=new personHead(gender)
    this.head=head
    this.addChild(head)
    this.setChildIndex(head,2)
    this.calculatePostion()
    //绑定事件
    this.touchEnabled=true;
    this.addEventListener(egret.TouchEvent.TOUCH_TAP,()=>{
      LL.GlobalEvents.emitEvents("switchToPerson",gender==="male"?{index:4}:{index:5})
    },this)
  }

  //创建身体
  private createBody(bodyName){
    if(this.body){
      this.removeChild(this.body)
    }
    this.bodyConfig=this.personConfig['body'][bodyName]
    let bodyData:egret.Texture=RES.getRes(bodyName);
    let body:eui.Image=new eui.Image(bodyData);
    body.anchorOffsetX=bodyData.bitmapData.width/2;
    body.anchorOffsetY=bodyData.bitmapData.height/2;
    body.x=this.width/2;
    body.y=this.height/2;
    this.body=body
    this.addChild(body)
    this.setChildIndex(body,1)
  }
  //创建腿
  private createLeg(legName){
    if(this.leg){
      this.removeChild(this.leg)
    }
    let legConfig=this.personConfig['leg'][legName]
    let legData:egret.Texture=RES.getRes(legName);
    let leg:eui.Image=new eui.Image(legData);
    leg.anchorOffsetX=legConfig.anchorOffsetX;
    leg.anchorOffsetY=legConfig.anchorOffsetY;
    this.leg=leg
    this.addChild(leg)
    this.setChildIndex(leg,0)
  }
  //计算位置
  private calculatePostion(){
    let source:egret.Texture=this.body.source as egret.Texture
    this.head.x=this.width/2-source.bitmapData.width/2+this.bodyConfig.headX;
    this.head.y=this.height/2-source.bitmapData.height/2+this.bodyConfig.headY;
    this.leg.x=this.width/2-source.bitmapData.width/2+this.bodyConfig.legX
    this.leg.y=this.height/2-source.bitmapData.height/2+this.bodyConfig.legY;
  }

  //改变装扮
  public changePerson(e){
    if(e.groupName==="body"){
     this.createBody(e.name)
    } else if(e.groupName==="leg"){
      this.createLeg(e.name)
    }else{
      this.head.createElement(e)
    }
    this.calculatePostion()
  }
}