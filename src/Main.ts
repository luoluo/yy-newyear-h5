//////////////////////////////////////////////////////////////////////////////////////
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  H5主舞台
//////////////////////////////////////////////////////////////////////////////////////

class Main extends eui.UILayer {
  protected createChildren(): void {
    super.createChildren();

    egret.lifecycle.addLifecycleListener(context => {
      // custom lifecycle plugin
    });

    egret.lifecycle.onPause = () => {
      egret.ticker.pause();
    };

    egret.lifecycle.onResume = () => {
      egret.ticker.resume();
    };

    this.runGame().catch(e => {
      console.error("游戏创建出错！");
      console.error(e);
    });
  }

  private async runGame() {
    await this.loadResource();
    this.createGameScene();
    // await platform.login();
    // const userInfo = await platform.getUserInfo();
  }

  // 加载资源
  private async loadResource() {
    try {
      const loadingView = new LoadingUI();
      this.stage.addChild(loadingView);
      // await RES.loadConfig("resource/default.res.json", "resource/");
      await RES.loadConfig("resource/demo.json", "resource/");
      await RES.loadGroup("all", 0, loadingView);
      this.stage.removeChild(loadingView);
    } catch (e) {
      console.error("资源加载出错！");
      console.error(e);
    }
  }

  /**
   * 创建场景界面
   * Create scene interface
   */
  protected createGameScene(): void {
    //主舞台
    let Stage = new stage();
    this.addChild(Stage);
    //操作区
    let Menu = new menu();
    this.addChild(Menu);
  }
}
