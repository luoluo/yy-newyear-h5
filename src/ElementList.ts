/**
 * ★元素列表，可滚动★
 * @param groupName 组名
 * @param height 高度
 */

class elementList extends eui.Scroller{

  constructor(groupName:string,width:number,height:number,gender?){
    super()

    this.width=width;
    this.height=height;
    this.scrollPolicyH="ScrollPolicy.OFF"
    this.visible=false;

    let group = new eui.Group();
    let layout=new eui.TileLayout();
    layout.requestedColumnCount=4;
    group.width=width;
    group.layout=layout
    RES.getGroupByName(gender?gender+groupName:groupName).map((item)=>{
      const itemBox=new elementItem(item.name,groupName,gender)
      group.addChild(itemBox)
    })
    this.viewport=group
  }

  public setState(visible:boolean):void{
    this.visible=visible
  }
}